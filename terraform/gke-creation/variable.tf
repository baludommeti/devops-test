variable "gcp_project_id" {
	description = "Your GCP Project ID"
	default = "learn-221609"
}

variable "gcp_region" {
	description = "The region to deploy the cluster"
	default = "asia-southeast1"
}

variable "gcp_bucket_name" {
	description = "The name of the backend bucket"
	default = "gke-backend-tf"
}

variable "gcp_cluster_name" {
	description = "The name of the gke cluster"
	default = "devops-gke-cluster"
}
variable "gke_cluster_cidr_blocks_1_display_name" {
	description = "The name of the cider block"
	default = "devops-test-ip"
}

variable "gke_cluster_cidr_blocks_1_range" {
	description = "The range of the master cidr block"
	default = "182.74.236.251/32"
}
variable "gke_cluster_cidr_blocks_2_display_name" {
	description = "The name of the cider block"
	default = "public-ip"
}

variable "gke_cluster_cidr_blocks_2_range" {
	description = "The range of the master cidr block"
	default = "0.0.0.0/0"
}

variable "gke_network" {
	description = "Network for the gke cluster" 
	default = "devops-test-vpc"
}

variable "gke_subnetwork" {
	description = "The subnet of the gke cluster"
	default = "devops-test-sub"
}


variable "balamuralikrishna_ip_range" {
	description = "The range of the master cidr block"
	default = "175.101.36.18/32"
}

variable "Ray_ip_range" {
	description = "The range of the master cidr block"
	default = "104.56.114.248/32"
}

variable "master_ipv4_range" {
	description = "The range of the master ipv4 cidr block"
	default = "172.24.72.0/28"

}


variable "gke_node_pool_name" {
	description = "The name of the managed nodepool"
	default = "test-node-pool"
}

variable "node_pool_version" {
	description = "The version of the managed node pool"
	default = "1.13.7-gke.8"
}

variable "node_pool_machine_type" {
	description = "The machine type of the managed node pool"
	default = "n1-standard-2"
}

variable "node_pool_disk_type" {
	description = "The disk type of the managed node pool"
	default = "pd-standard"
}
	
variable "initial_node_count" {
	description = "The value of the initial node count"
	default = 1
}
variable "gke_enable_legacy_abac" {
	description = "The value of the enable_legacy_abac for gke"
	default = false
}
variable "gke_remove_default_node_pool" {
	description = "The value of the remove_default_node_pool flag for gke"
	default = true
}
variable "gke_issue_client_certificate" {
	description = "The value of the issue_client_certificate flag for gke"
	default = false
}
variable "gke_enable_private_nodes" {
	description = "The value of the enable_private_nodes flag for gke"
	default = true
}
variable "gke_istio_config_disabled" {
	description = "The value of the istio_config_disabled flag for gke"
	default = false
}
variable "gke_create_timeout" {
	description = "The value of the timeout for create in gke"
	default = "30m"
}
variable "gke_update_timeout" {
	description = "The value of the timeout for update in gke"
	default = "40m"
}
variable "managed_nodepool_auto_upgrade" {
	description = "The value of the auto_upgrade flag for managed node pool"
	default = false
}
variable "managed_nodepool_auto_repair" {
	description = "The value of the auto_repair flag for managed node pool"
	default = true
}
variable "managed_nodepool_preemptible" {
	description = "The value of the preemptible flag for managed node pool"
	default = false
}
variable "managed_nodepool_disk_size_gb" {
	description = "The value of the disk_size_gb for managed node pool"
	default = 100
}
variable "managed_label_env" {
	description = "The value of the managed_label_env for managed node pool"
	default = "poc"
}
variable "managed_label_role" {
	description = "The value of the managed_label_role for managed node pool"
	default = "app"
}
variable "managed_nodepool_disable_legacy_endpoints" {
	description = "The value of the managed_nodepool_disable_legacy_endpoints for managed node pool"
	default = "true"
}

variable "pool_node_count" {
	description = "The value of the pool node count"
	default = 1
}

variable "min_node_count" {
	description = "The value of the min node count"
	default = 1	
}

variable "max_node_count" {
	description = "The value of the max node count"
	default = 3
}


variable "backend_bucket" {
	description = "The name of backend bucket"
	default = "gke-backend-tf"
}
variable "backend_prefix" {
	description = "The value of backend prefix"
	default = "gke-tf-state"
}
variable "backend_credentials" {
	description = "The value of backend credentials"
	default = "../account.json"
}

variable "pod_secondary_range_name" {
	description = "The name of the secondary range of the cluster subnet for the pods"
	default = "devops-test-sub"
}

variable "svcs_secondary_range_name" {
	description = "The name of the secondary range of the cluster subnet for the services"
	default = "devops-test-sub"
}
