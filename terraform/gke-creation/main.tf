provider "google-beta" {
  credentials = "${file("../account.json")}" 
  project     = "${var.gcp_project_id}"
  region      = "${var.gcp_region}"
}

/*resource "google_storage_bucket" "gke_tf_bucket" {
  project = "${var.gcp_project_id}"
  name    = "${var.gcp_bucket_name}"
}*/

/*resource "google_storage_bucket" "spinnaker_data" {
  project  = "${var.gcp_project_id}"
  name     = "${var.spinnaker_bucket_name}"
  location = "${var.gcp_region}"
}*/

resource "google_container_cluster" "et-gke" {
  provider           = "google-beta"
  project            = "${var.gcp_project_id}"
  name               = "${var.gcp_cluster_name}"
  location           = "${var.gcp_region}"
  initial_node_count = "${var.initial_node_count}"
  enable_legacy_abac = "${var.gke_enable_legacy_abac}"
  network            = "${var.gke_network}"
  subnetwork         = "${var.gke_subnetwork}"
  remove_default_node_pool = "${var.gke_remove_default_node_pool}"

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = "${var.gke_issue_client_certificate}"
    }
  }

  master_authorized_networks_config {

    cidr_blocks {
      cidr_block   = "${var.gke_cluster_cidr_blocks_1_range}"
      display_name = "${var.gke_cluster_cidr_blocks_1_display_name}"
    }
    
    cidr_blocks {
      cidr_block   = "${var.gke_cluster_cidr_blocks_2_range}"
      display_name = "${var.gke_cluster_cidr_blocks_2_display_name}"
    }

  }

  private_cluster_config {
    enable_private_nodes   = "${var.gke_enable_private_nodes}"
    master_ipv4_cidr_block = "${var.master_ipv4_range}"
  }


  ip_allocation_policy {
    cluster_secondary_range_name  = "${var.pod_secondary_range_name}"
    services_secondary_range_name = "${var.svcs_secondary_range_name}"
  }

  addons_config {
    istio_config {
      disabled = "${var.gke_istio_config_disabled}"
    }
  }

  timeouts {
    create = "${var.gke_create_timeout}"
    update = "${var.gke_update_timeout}"
  }
}


resource "google_container_node_pool" "et_managed_node_pool" {

  provider   = "google-beta"
  project    = "${var.gcp_project_id}"
  name       = "${var.gke_node_pool_name}"
  location   = "${var.gcp_region}"
  cluster    = "${google_container_cluster.et-gke.name}"
  version    = "${var.node_pool_version}"
  node_count = "${var.pool_node_count}"

  autoscaling {
    min_node_count = "${var.min_node_count}"
    max_node_count = "${var.max_node_count}"
  }

  management {
    auto_upgrade = "${var.managed_nodepool_auto_upgrade}"
    auto_repair  = "${var.managed_nodepool_auto_repair}"
  }

  node_config {
    preemptible  = "${var.managed_nodepool_preemptible}"
    machine_type = "${var.node_pool_machine_type}"
    disk_size_gb = "${var.managed_nodepool_disk_size_gb}"
    disk_type    = "${var.node_pool_disk_type}"

    labels = {
      env  = "${var.managed_label_env}"
      role = "${var.managed_label_role}"
    }

    metadata = {
      disable-legacy-endpoints = "${var.managed_nodepool_disable_legacy_endpoints}"
    }
    tags = ["poc", "app"]

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}
