###### Cloud NAT For ap southeast-1#######
resource "google_compute_router" "test_router" {
  name    = "${var.router_name}"
  region  = "${var.region}"
  network = "devops-test-vpc"

  bgp {
    asn = "64514"
  }

  timeouts {
    create = "5m"
  }
}


// Create the Pub ip for Cloud Nat
resource "google_compute_address" "test_address" {
  name   = "test-nat-external-address"
  region = "${var.region}"
}

resource "google_compute_router_nat" "test-cloud-nat" {
  name                   = "cloud-nat-test"
  router                 = "${var.router_name}"
  region                 = "${var.region}"
  nat_ip_allocate_option = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = "${google_compute_subnetwork.private_subnet.name}"
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}
