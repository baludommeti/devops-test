resource "google_compute_firewall" "allow-http" {
  name    = "fw-allow-http"
  network = "${google_compute_network.vpc.name}"
allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges  = ["175.101.36.18/32", "104.56.114.248/32"]
  target_tags    = ["web-server"]
}
resource "google_compute_firewall" "allow-ssh" {
  name    = "fw-allow-ssh"
  network = "${google_compute_network.vpc.name}"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges  = ["175.101.36.18/32"]
  target_tags    = ["web-server"]
  }
resource "google_compute_firewall" "allow-https" {
  name    = "fw-allow-https"
  network = "${google_compute_network.vpc.name}"
allow {
    protocol = "tcp"
    ports    = ["443"]
  }
  source_ranges  = ["175.101.36.18/32", "104.56.114.248/32"]
  target_tags    = ["web-server"]
}

