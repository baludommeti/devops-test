variable "region" {
  default = "asia-southeast1"
}

variable "app" {
  default = "web-server"
}

variable "role" {
  default = "server"
}

variable "zone" {
  default = "asia-southeast1-a"
}

variable "project" {
  default = "learn-221609"
}

variable "ssh_user" {
  default = "balamuralikrishnadommeti"
}

variable "ssh_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "ssh_user_ray" {
  default = "ray"
}

variable "ssh_key_ray" {
  default = "/Users/balamuralikrishnadommeti/Documents/devops-test/ray-key.pub"
}

variable "router_name" {
  default = "test-router"
}

