resource "google_compute_address" "external" {
  name = "external-ip"
}

resource "google_compute_instance" "web_server" {
  project      = "${var.project}"
  count        = "1"
  name         = "${var.app}"
  machine_type = "f1-micro"
  zone         = "${var.zone}"
  tags         = ["${var.app}", "${var.project}"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
      size  = "20"
      type  = "pd-ssd"
    }
  }

labels = {
      app     = "${var.app}"
      region  = "${var.region}"
      zone    = "${var.zone}"
      project = "${var.project}"
    
    }
network_interface {
    subnetwork = "${google_compute_subnetwork.private_subnet.name}"
    access_config {
        nat_ip = "${google_compute_address.external.address}"
        }
  }
    metadata = {
    sshKeys = "${var.ssh_user}:${file(var.ssh_key)}"
    sshKeys1 = "${var.ssh_user_ray}:${file(var.ssh_key_ray)}"
}
}

