resource "google_compute_network" "vpc" {
  name          = "devops-test-vpc"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
}
resource "google_compute_subnetwork" "private_subnet" {
  name          =  "devops-test-sub"
  ip_cidr_range = "10.172.0.0/16"
  network      = "${google_compute_network.vpc.name}"
  region        = "${var.region}"
}
